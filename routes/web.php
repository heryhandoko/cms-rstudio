<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
Auth::routes();

Route::group(['middleware' => ['user.check', 'auth', 'web']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'masters'], function () {

        Route::get('servers', 'ServersController@index')->name('masters.servers.index');
        Route::get('servers/fetch', 'ServersController@fetch')->name('masters.servers.fetch');
        Route::post('servers/store', 'ServersController@store')->name('masters.servers.store');
        Route::post('servers/update', 'ServersController@update')->name('masters.servers.update');
        Route::get('servers/{id}/edit', 'ServersController@edit')->name('masters.servers.edit');
        Route::get('servers/{id}/show', 'ServersController@show')->name('masters.servers.show');
        Route::delete('servers/destroy', 'ServersController@destroy')->name('masters.servers.destroy');

        Route::get('medialive', 'MedialiveController@index')->name('masters.medialive.index');
        Route::get('medialive/fetch', 'MedialiveController@fetch')->name('masters.medialive.fetch');
        Route::post('medialive/store', 'MedialiveController@store')->name('masters.medialive.store');
        Route::post('medialive/update', 'MedialiveController@update')->name('masters.medialive.update');
        Route::get('medialive/{id}/edit', 'MedialiveController@edit')->name('masters.medialive.edit');
        Route::get('medialive/{id}/show', 'MedialiveController@show')->name('masters.medialive.show');
        Route::delete('medialive/destroy', 'MedialiveController@destroy')->name('masters.medialive.destroy');

        Route::get('missedevent', 'MissedeventController@index')->name('masters.missedevent.index');
        Route::get('missedevent/fetch', 'MissedeventController@fetch')->name('masters.missedevent.fetch');
        Route::post('missedevent/store', 'MissedeventController@store')->name('masters.missedevent.store');
        Route::post('missedevent/update', 'MissedeventController@update')->name('masters.missedevent.update');
        Route::get('missedevent/{id}/edit', 'MissedeventController@edit')->name('masters.missedevent.edit');
        Route::get('missedevent/{id}/show', 'MissedeventController@show')->name('masters.missedevent.show');
        Route::delete('missedevent/destroy', 'MissedeventController@destroy')->name('masters.missedevent.destroy');

        Route::get('logos', 'LogosController@index')->name('masters.logos.index');
        Route::get('logos/fetch', 'LogosController@fetch')->name('masters.logos.fetch');
        Route::post('logos/store', 'LogosController@store')->name('masters.logos.store');
        Route::post('logos/update', 'LogosController@update')->name('masters.logos.update');
        Route::get('logos/{id}/edit', 'LogosController@edit')->name('masters.logos.edit');
        Route::get('logos/{id}/show', 'LogosController@show')->name('masters.logos.show');
        Route::delete('logos/destroy', 'LogosController@destroy')->name('masters.logos.destroy');

        Route::get('logolive', 'LogoliveController@index')->name('masters.logolive.index');
        Route::get('logolive/fetch', 'LogoliveController@fetch')->name('masters.logolive.fetch');
        Route::post('logolive/store', 'LogoliveController@store')->name('masters.logolive.store');
        Route::post('logolive/update', 'LogoliveController@update')->name('masters.logolive.update');
        Route::get('logolive/{id}/edit', 'LogoliveController@edit')->name('masters.logolive.edit');
        Route::get('logolive/{id}/show', 'LogoliveController@show')->name('masters.logolive.show');
        Route::delete('logolive/destroy', 'LogoliveController@destroy')->name('masters.logolive.destroy');
    });

    Route::group(['prefix' => 'operations'], function () {
        Route::get('servers/{slug}', 'ServersController@strstop')->name('operations.servers');
        Route::post('servers/start', 'ServersController@start')->name('operations.servers.start');
        Route::get('medialive', 'MedialiveController@strstop')->name('operations.medialive');
        Route::get('missedevent', 'MedialiveController@strstop')->name('operations.missedevent');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('users', 'UsersController@index')->name('settings.users.index');
        Route::get('users/fetch', 'UsersController@fetch')->name('settings.users.fetch');
        Route::get('users/{id}/edit', 'UsersController@edit')->name('settings.users.edit');
        Route::get('users/{id}/show', 'UsersController@show')->name('settings.users.show');
        Route::post('users/store', 'UsersController@store')->name('settings.users.store');
        Route::post('users/update', 'UsersController@update')->name('settings.users.update');
        Route::get('users/profile', 'UsersController@profile')->name('settings.users.profile');
        Route::delete('users/destroy', 'UsersController@destroy')->name('settings.users.destroy');

        Route::get('roles', 'RolesController@index')->name('settings.roles.index');
        Route::get('roles/create', 'RolesController@create')->name('settings.roles.create');
        Route::get('roles/fetch', 'RolesController@fetch')->name('settings.roles.fetch');
        Route::get('roles/{id}/edit', 'RolesController@edit')->name('settings.roles.edit');
        Route::get('roles/{id}/show', 'RolesController@show')->name('settings.roles.show');
        Route::post('roles/store', 'RolesController@store')->name('settings.roles.store');
        Route::post('roles/update', 'RolesController@update')->name('settings.roles.update');
        Route::post('roles/save', 'RolesController@save')->name('settings.roles.save');
        Route::delete('roles/destroy', 'RolesController@destroy')->name('settings.roles.destroy');

        Route::get('modules', 'ModulesController@index')->name('settings.modules.index');
        Route::get('modules/fetch', 'ModulesController@fetch')->name('settings.modules.fetch');
        Route::get('modules/{id}/edit', 'ModulesController@edit')->name('settings.modules.edit');
        Route::get('modules/{id}/show', 'ModulesController@show')->name('settings.modules.show');
        Route::post('modules/store', 'ModulesController@store')->name('settings.modules.store');
        Route::post('modules/update', 'ModulesController@update')->name('settings.modules.update');
        Route::delete('modules/destroy', 'ModulesController@destroy')->name('settings.modules.destroy');

        Route::get('menus', 'MenusController@index')->name('settings.menus.index');
        Route::put('menus/update', 'MenusController@update')->name('settings.menus.update');
        Route::post('menus/store', 'MenusController@store')->name('settings.menus.store');
        Route::delete('menus/destroy/{id}', 'MenusController@destroy')->name('settings.menus.destroy');

        Route::get('configs', 'ConfigsController@index')->name('settings.configs.index');
        Route::post('configs/store', 'ConfigsController@store')->name('settings.configs.store');
    });
});
