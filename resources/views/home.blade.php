@extends('page')
@section('title', 'Dashboard')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li class="active">Dashboard</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Dashboard <small>R+ Studio</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<!-- begin row -->
<div class="row">
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-green">
            <div class="stats-icon"><i class="fa fa-desktop"></i></div>
            <div class="stats-info">
                <h4>TOTAL VISITORS</h4>
                <p>0</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-blue">
            <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
            <div class="stats-info">
                <h4>BOUNCE RATE</h4>
                <p>0.00%</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-purple">
            <div class="stats-icon"><i class="fa fa-users"></i></div>
            <div class="stats-info">
                <h4>UNIQUE VISITORS</h4>
                <p>{{ $users }}</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
            <div class="stats-info">
                <h4>AVG TIME ON SITE</h4>
                <p>00:00:00</p>
            </div>
            <div class="stats-link">
                <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
</div>
<!-- end row -->
<!-- begin row -->
<div class="row">
    <!-- begin col-8 -->
    <div class="col-md-8 ui-sortable">
        <div class="panel panel-inverse" data-sortable-id="index-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Website Analytics (Last 7 Days)</h4>
            </div>
            <div class="panel-body">
                <div id="interactive-chart" class="height-sm" style="padding: 0px; position: relative;"><canvas class="flot-base" width="1488" height="600" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 744px; height: 300px;"></canvas>
                    <div class="flot-text" style="position: absolute; inset: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                        <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; inset: 0px; display: block;">
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 80px; text-align: center;">May&nbsp;15</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 191px; text-align: center;">May&nbsp;19</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 301px; text-align: center;">May&nbsp;22</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 412px; text-align: center;">May&nbsp;25</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 522px; text-align: center;">May&nbsp;28</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 37px; top: 283px; left: 633px; text-align: center;">May&nbsp;31</div>
                        </div>
                        <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; inset: 0px; display: block;">
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 270px; left: 13px; text-align: right;">0</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 243px; left: 7px; text-align: right;">20</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 216px; left: 7px; text-align: right;">40</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 189px; left: 7px; text-align: right;">60</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 162px; left: 7px; text-align: right;">80</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 1px; text-align: right;">100</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 108px; left: 1px; text-align: right;">120</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 81px; left: 1px; text-align: right;">140</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 54px; left: 1px; text-align: right;">160</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 27px; left: 1px; text-align: right;">180</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 1px; text-align: right;">200</div>
                        </div>
                    </div><canvas class="flot-overlay" width="1488" height="600" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 744px; height: 300px;"></canvas>
                    <div class="legend">
                        <div style="position: absolute; width: 87px; height: 46px; top: 18px; right: 29px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                        <table style="position:absolute;top:18px;right:29px;;font-size:smaller;color:#545454">
                            <tbody>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ddd;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #348fe2;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Page Views</td>
                                </tr>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ddd;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #00acac;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Visitors</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-inverse" data-sortable-id="index-10">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Calendar</h4>
            </div>
            <div class="panel-body">
                <div id="datepicker-inline" class="datepicker-full-width">
                    <div></div>
                </div>
            </div>
        </div>

    </div>
    <!-- end col-8 -->
    <!-- begin col-4 -->
    <div class="col-md-4 ui-sortable">
        <div class="panel panel-inverse" data-sortable-id="index-6">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Servers Status</h4>
            </div>
            <div class="panel-body p-t-0">
                <table class="table table-valign-middle m-b-0">
                    <tbody>
                        @foreach($servers as $key => $server)
                        <tr>
                            <td>{{ $server->name }}</td>
                            <td><i class="fa fa-fw m-r-5 fa-circle text-success"></i></td>
                            <td width="250">
                                <a href="javascript:;" class="btn btn-success btn-xs m-r-1"><i class="fa fa-play"></i> Start</a>
                                <a href="javascript:;" class="btn btn-danger btn-xs m-r-1"><i class="fa fa-stop"></i> Stop</a>
                                <a href="javascript:;" class="btn btn-warning btn-xs"><i class="fa fa-refresh"></i> Restart</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel panel-inverse" data-sortable-id="index-7">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Visitors User Agent</h4>
            </div>
            <div class="panel-body">
                <div id="donut-chart" class="height-sm" style="padding: 0px; position: relative;"><canvas class="flot-base" width="694" height="600" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 347px; height: 300px;"></canvas><canvas class="flot-overlay" width="694" height="600" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 347px; height: 300px;"></canvas>
                    <div class="legend">
                        <div style="position: absolute; width: 71px; height: 108px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                        <table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454">
                            <tbody>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ccc;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #5b6392;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Chrome</td>
                                </tr>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ccc;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #727cb6;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Firefox</td>
                                </tr>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ccc;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #8e96c5;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Safari</td>
                                </tr>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ccc;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #348fe2;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Opera</td>
                                </tr>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border:1px solid #ccc;padding:1px">
                                            <div style="width:4px;height:0;border:5px solid #1993E4;overflow:hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">IE</td>
                                </tr>
                            </tbody>
                        </table>
                    </div><span class="pieLabel" id="pieLabel0" style="position: absolute; top: 81.5px; left: 247px;">
                        <div style="font-size:x-small;text-align:center;padding:2px;color:#5b6392;">Chrome<br>37%</div>
                    </span><span class="pieLabel" id="pieLabel1" style="position: absolute; top: 260.5px; left: 111px;">
                        <div style="font-size:x-small;text-align:center;padding:2px;color:#727cb6;">Firefox<br>32%</div>
                    </span><span class="pieLabel" id="pieLabel2" style="position: absolute; top: 122.5px; left: 6.5px;">
                        <div style="font-size:x-small;text-align:center;padding:2px;color:#8e96c5;">Safari<br>16%</div>
                    </span><span class="pieLabel" id="pieLabel3" style="position: absolute; top: 32.5px; left: 54.5px;">
                        <div style="font-size:x-small;text-align:center;padding:2px;color:#348fe2;">Opera<br>11%</div>
                    </span><span class="pieLabel" id="pieLabel4" style="position: absolute; top: 6.5px; left: 120px;">
                        <div style="font-size:x-small;text-align:center;padding:2px;color:#1993E4;">IE<br>5%</div>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-4 -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

</div>
<!-- end row -->
@endsection
@section('js')
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script> -->
<script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sparkline/jquery.sparkline.js') }}"></script>
<!-- <script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> -->
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/js/dashboard.min.js') }}?v=<?=date('YmdHis') ?>"></script>
<!-- <script src="{{ asset('assets/js/apps.min.js') }}"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->
<script>
    $(document).ready(function() {
        Dashboard.init();
    });
</script>
@endsection