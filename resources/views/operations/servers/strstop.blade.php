@extends('page')
@section('title', 'Servers')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li class="active">Operations</li>
    <li class="active">Servers</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Servers <small>Start/Stop</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-2">
            <div class="hidden-sm hidden-xs">
                <h5 class="m-t-20">Servers List</h5>
                <ul class="nav nav-pills nav-stacked nav-inbox">
                    @foreach($servers as $key => $server)
                    <li class="{{ ($server->name == $detail->name) ?'active':'' }}">
                        <a href="{{ url('operations/servers/'.$server->filename ) }}">
                            <i class="fa fa-server fa-fw m-r-5"></i> {{ $server->name }} <i class="fa fa-fw m-r-5 fa-circle {{ $key==0?'text-success':'text-danger' }} pull-right"></i>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-10">
            <!-- begin wrapper -->
            <div class="wrapper bg-white">
                <h4 class="m-b-15 m-t-0 p-b-10 underline"><i class="fa fa-fw m-r-5 fa-circle text-success"></i>{{ ucfirst($detail->name) }}</h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <div class="btn-group m-r-5">
                        <a href="#" class="btn btn-white btn-sm m-r-5" disabled><i class="fa fa-link m-r-5"></i>Connected</a>
                        <a class="btn btn-white btn-sm m-r-5 btn-action-start" data-id="{{ $detail->id }}"><i class="fa fa-play m-r-5"></i>Start</a>
                        <a class="btn btn-white btn-sm m-r-5 btn-action-stop"><i class="fa fa-stop m-r-5"></i>Stop</a>
                        <a class="btn btn-white btn-sm m-r-5 btn-action-refresh"><i class="fa fa-refresh m-r-5"></i>Refresh</a>
                    </div>
                </ul>
                <div class="row">
                    <div class="col-md-2">
                        <p>Status</p>
                    </div>
                    <div class="col-md-3">
                        <p>: <span class="label label-success">Running</span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>File Name</p>
                    </div>
                    <div class="col-md-3">
                        <p>: {{ $detail->filename }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>Filepath</p>
                    </div>
                    <div class="col-md-3">
                        <p>: {{ $detail->filename_path }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>Start Time</p>
                    </div>
                    <div class="col-md-3">
                        <p>: 00:00:00 </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>History</p>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Operation Name</th>
                            <th>Status</th>
                            <th>Time</th>
                            <th>Time stamp</th>
                            <th>User</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- end wrapper -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('body').on('click', 'a.btn-action-start', function(e) {
            var _button = $(this);
            _button.html('<i class="fa fa-spinner fa-spin"></i> Starting...');
            _button.prop('disabled', true);
            e.preventDefault();
            var _id = _button.attr('data-id');
            var _method = 'POST';
            var _url = "{{ route('operations.servers.start') }}";
            $.ajax({
                url: _url,
                type: _method,
                data: {
                    id: _id,
                },
                success: function(result) {
                    // if (result.success) {
                    //     $('#modal-dialog').modal('toggle');
                    //     $('#data-table').DataTable().ajax.reload();
                    // }
                },
                error: function(err) {
                    $.each(JSON.parse(err.responseText).message, function(i, error) {
                        // var _field = $(document).find('[name="' + i + '"]');
                        // _field.addClass('is-invalid');
                        // var el = $(document).find('[class="invalid-feedback invalid-' + i + '"]');
                        // el.css('display', 'block');
                        // el.text(error[0]);
                    });
                    _button.html('<i class="fa fa-play m-r-5"></i> Start');
                    _button.prop('disabled', false);
                }
            });
        });
    });
</script>
@stop