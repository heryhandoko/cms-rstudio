@extends('page')
@section('title', 'Media Live')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li class="active">Operations</li>
    <li class="active">Media Live</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Media Live <small>Start/Stop</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-2">
            <div class="hidden-sm hidden-xs">
                <h5 class="m-t-20">Servers List</h5>
                <ul class="nav nav-pills nav-stacked nav-inbox">
                    @foreach($servers as $key => $server)
                    <li class="{{ $key==0?'active':'' }}">
                        <a href="#">
                            <i class="fa fa-server fa-fw m-r-5"></i> {{ $server->name }} <i class="fa fa-fw m-r-5 fa-circle {{ $key==0?'text-success':'text-danger' }} pull-right"></i>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-10">
            <!-- begin wrapper -->
            <div class="wrapper bg-white">
                <h4 class="m-b-15 m-t-0 p-b-10 underline"><i class="fa fa-fw m-r-5 fa-circle text-success"></i>Jigen</h4>
                <ul class="media-list underline m-b-20 p-b-15">
                    <div class="btn-group m-r-5">
                        <a href="#" class="btn btn-white btn-sm m-r-5"><i class="fa fa-unlink m-r-5"></i>Connect</a>
                        <a href="#" class="btn btn-white btn-sm m-r-5"><i class="fa fa-play m-r-5"></i>Start</a>
                        <a href="#" class="btn btn-white btn-sm m-r-5"><i class="fa fa-stop m-r-5"></i>Stop</a>
                        <a href="#" class="btn btn-white btn-sm m-r-5"><i class="fa fa-refresh m-r-5"></i>Refresh</a>
                    </div>
                </ul>
                <div class="row">
                    <div class="col-md-2">
                        <p>Status</p>
                    </div>
                    <div class="col-md-3">
                        <p>: Running</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>File Name</p>
                    </div>
                    <div class="col-md-3">
                        <p>: start-jigen.sh</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>Filepath</p>
                    </div>
                    <div class="col-md-3">
                        <p>: /home/obenk/start-jigen.sh</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <p>Start Time</p>
                    </div>
                    <div class="col-md-3">
                        <p>: - </p>
                    </div>
                </div>
            </div>
            <!-- end wrapper -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
@endsection