@extends('layouts.app')

@section('body')
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    @include('partials.header')
    @include('partials.sidebar')
    <div id="content" class="content">
        @yield('content_header')
        @yield('content')
    </div>
</div>
@stop