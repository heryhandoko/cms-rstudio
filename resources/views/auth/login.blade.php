@extends('layouts.app')
@section('title', 'Login')
@section('classes_body'){{ 'pace-top bg-white' }}@stop
@section('css')
<style>
    .img-icon-login {
        height: 32px;
        position: relative;
        font-size: 0;
        margin-right: 5px;
        top: -5px;
    }
</style>
@stop
@section('body')
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-with-news-feed">
        <!-- begin news-feed -->
        <div class="news-feed">
            <div class="news-image">
                <img src="{{ asset('assets/img/login-bg/bg-8.jpg') }}" data-id="login-cover-image" alt="Background Login" />
            </div>
            <div class="news-caption">
                <h4 class="caption-title">
                    <img class="img-icon-login" src="{{ asset('assets/img/rcti-plus-logo.png') }}" data-id="rcti-plus-icon" alt="RCTI Plus Icon" />
                </h4>
                <p>
                    © RCTI+ All Right Reserved 2022
                </p>
            </div>
        </div>
        <!-- end news-feed -->
        <!-- begin right-content -->
        <div class="right-content">
            <!-- begin login-header -->
            <div class="login-header">
                <div class="brand">
                    <!-- <span class="logo"></span> R+ Studio -->
                    <img class="img-icon-login" src="{{ asset('assets/img/rcti-plus-icon.png') }}" data-id="rcti-plus-logo" alt="RCTI Plus Logo" />Studio
                    <small>Sign in to start your session</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end login-header -->
            <!-- begin login-content -->
            <div class="login-content">
                <form method="POST" action="{{ route('login') }}" class="margin-bottom-0">
                    @csrf
                    <div class="form-group m-b-15">
                        <input id="email" type="email" class="form-control input-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email Address">
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group m-b-15">
                        <input id="password" type="password" class="form-control input-lg @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="Password">
                        @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="checkbox m-b-30">
                        <label>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} /> {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="button" class="btn btn-primary btn-block btn-lg btn-submit">
                            {{ __('Login') }} <i class="fa fa-sign-in"></i>
                        </button>
                    </div>
                    <!-- <div class="m-t-20 m-b-40 p-b-40 text-inverse">
                            Not a member yet? Click <a href="register_v3.html" class="text-success">here</a> to register.
                        </div> -->
                    <hr />
                    <p class="text-center">
                        &copy; RCTI+ All Right Reserved 2022
                    </p>
                </form>
            </div>
            <!-- end login-content -->
        </div>
        <!-- end right-container -->
    </div>
    <!-- end login -->
</div>
<!-- end page container -->
@endsection
@section('js')
<script>
    $(document).ready(function() {
        App.init();
        $("button.btn-submit").click(function(event) {
            $('button.btn-submit').html('<i class="fa fa-spinner fa-spin"></i> Processing...');
            $('button.btn-submit').prop('disabled', true);
            $("form").submit();
            event.preventDefault();
        });
    });

    $(document).on("keypress", "input#password", function(e) {
        if (e.which == 13) {
            $("button.btn-submit").click();
        }
    });

    $(document).on("keypress", "input#email", function(e) {
        if (e.which == 13) {
            $("input#password").focus();
        }
    });
</script>
@endsection