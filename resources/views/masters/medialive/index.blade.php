@extends('page')
@section('title', 'Medialive')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li><a href="{{ url('/') }}">Masters</a></li>
    <li class="active">Medialive</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Medialive <small>List</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="#modal-dialog" class="btn btn-xs btn-icon btn-circle btn-primary btn-action-add" data-toggle="modal"><i class="fa fa-plus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Server - list</h4>
            </div>
            <div class="panel-body">
                <table id="data-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Filename</th>
                            <th>Path</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->
<!-- #modal-dialog -->
<div class="modal fade" id="modal-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Server</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('id' => 'MyForm', 'enctype'=>"multipart/form-data",'name'=>'MyForm', 'class'=>'form-horizontal')) }}
                <input type="hidden" name="id" id="id" />
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Name" name="name" id="name" />
                        <div class="invalid-feedback invalid-name"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Filename</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Filename" name="filename" id="filename" />
                        <div class="invalid-feedback invalid-filename"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Filename Path</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Filename Path" name="filename_path" id="filename_path" />
                        <div class="invalid-feedback invalid-filename_path"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Description" name="description" id="description" />
                        <div class="invalid-feedback invalid-description"></div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white btn-action-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                <button class="btn btn-sm btn-success btn-action-submit"><i class="fa fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
@stop
@section('js')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('masters.medialive.fetch') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'filename',
                    name: 'filename'
                },
                {
                    data: 'filename_path',
                    name: 'filename_path'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ],
        });

        $("#modal-dialog").on('show.bs.modal', function() {
            $('button.btn-action-submit').html('<i class="fa fa-paper-plane"></i> Submit');
            $('button.btn-action-submit').prop('disabled', false);
            $('.form-control').removeClass('is-invalid');
            $("form#MyForm :input").each(function() {
                var inputName = $(this).attr('name');
                $('.invalid-' + inputName).text('');
                if (inputName !== undefined && inputName !== '_token') {
                    var _field = $(document).find('[name="' + inputName + '"]');
                    _field.val('');
                    _field.attr('disabled', false);
                }
            });
            $('form#MyForm')[0].reset();
            $('button.btn-action-submit').show();
        });

        $('button.btn-action-submit').click(function(e) {
            $(this).html('<i class="fa fa-spinner fa-spin"></i> Processing...');
            $(this).prop('disabled', true);
            e.preventDefault();
            $('.form-control').removeClass('is-invalid');
            $("form#MyForm :input").each(function() {
                var inputName = $(this).attr('name');
                $('.invalid-' + inputName).text('');
            });
            var _form = $("form#MyForm");
            var formData = new FormData(_form[0]);
            var _id = $('input#id').val();
            var _method = 'POST';
            var _url = "{{ route('masters.medialive.store') }}";
            if (_id) {
                _url = "{{ route('masters.medialive.update') }}";
            }
            $.ajax({
                url: _url,
                type: _method,
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                success: function(result) {
                    if (result.success) {
                        $('#modal-dialog').modal('toggle');
                        $('#data-table').DataTable().ajax.reload();
                    }
                },
                error: function(err) {
                    $.each(JSON.parse(err.responseText).message, function(i, error) {
                        var _field = $(document).find('[name="' + i + '"]');
                        _field.addClass('is-invalid');
                        var el = $(document).find('[class="invalid-feedback invalid-' + i + '"]');
                        el.css('display', 'block');
                        el.text(error[0]);
                    });
                    $('button.btn-action-submit').html('<i class="fa fa-paper-plane"></i> Submit');
                    $('button.btn-action-submit').prop('disabled', false);
                }
            });
        });

        $('body').on('click', '.btn-action-edit', function() {
            var id = $(this).data("id");
            $('#modal-dialog').modal('toggle');
            $.get('/masters/medialive/' + id + '/edit', function(data) {
                $('.modal-title').html("Edit Medialive");
                $("form#MyForm :input").each(function() {
                    var inputName = $(this).attr('id');
                    if (inputName !== undefined) {
                        var _field = $(document).find('[name="' + inputName + '"]');
                        _field.val(data[inputName]);
                        _field.attr('disabled', false);
                    }
                });
            });
        });

        $('body').on('click', '.btn-action-view', function() {
            var id = $(this).data("id");
            $('#modal-dialog').modal('toggle');
            $('button.btn-action-submit').hide();
            $.get('/masters/medialive/' + id + '/show', function(data) {
                $('.modal-title').html("View Medialive");
                $("form#MyForm :input").each(function() {
                    var inputName = $(this).attr('id');
                    if (inputName !== undefined) {
                        var _field = $(document).find('[name="' + inputName + '"]');
                        _field.val(data[inputName]);
                        _field.attr('disabled', true);
                    }
                });
            });
        });

        $('body').on('click', '.btn-action-delete', function() {
            var id = $(this).data("id");
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(result) {
                    if (result) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('masters.medialive.destroy') }}",
                            dataType: 'JSON',
                            data: {
                                'id': id,
                            },
                            success: function(data) {
                                swal("Deleted!", "Your medialive has been deleted.", "success");
                                $('#data-table').DataTable().ajax.reload();
                            }
                        });
                    }
                });
        });
    });
</script>
@stop