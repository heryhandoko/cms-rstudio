@extends('page')
@section('title', 'Profile')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li class="active">Profile</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Profile <small>Page</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<!-- begin profile-container -->
<div class="profile-container">
    <!-- begin profile-section -->
    <div class="profile-section">
        <!-- begin profile-left -->
        <div class="profile-left">
            <!-- begin profile-image -->
            <div class="profile-image">
                <img src="{{ asset('assets/img/profile-cover.jpg') }}" />
                <i class="fa fa-user hide"></i>
            </div>
            <!-- end profile-image -->
            <div class="m-b-10">
                <a href="#" class="btn btn-warning btn-block btn-sm">Change Picture</a>
            </div>
        </div>
        <!-- end profile-left -->
        <!-- begin profile-right -->
        <div class="profile-right">
            <!-- begin profile-info -->
            <div class="profile-info">
                <!-- begin table -->
                <div class="table-responsive">
                    <table class="table table-profile">
                        <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h4>{{ Auth::user()->name }} <small>{{ Auth::user()->job_title }}</small></h4>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="highlight">
                                <td class="field">Job Title</td>
                                <td><a href="#">{{ Auth::user()->job_title }}</a></td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td class="field">Mobile</td>
                                <td><i class="fa fa-mobile fa-lg m-r-5"></i> +62 813 8000 1903 <a href="#" class="m-l-5">Edit</a></td>
                            </tr>
                            <tr>
                                <td class="field">Home</td>
                                <td><a href="#">Add Number</a></td>
                            </tr>
                            <tr>
                                <td class="field">Office</td>
                                <td><a href="#">Add Number</a></td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">About Me</td>
                                <td><a href="#">Add Description</a></td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td class="field">Country/Region</td>
                                <td>
                                    <select class="form-control input-inline input-xs" name="region">
                                        <option value="ID" selected>Indonesia</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="field">City</td>
                                <td>Jakarta</td>
                            </tr>
                            <tr>
                                <td class="field">State</td>
                                <td><a href="#">Add State</a></td>
                            </tr>
                            <tr>
                                <td class="field">Website</td>
                                <td><a href="#">Add Webpage</a></td>
                            </tr>
                            <tr>
                                <td class="field">Gender</td>
                                <td>
                                    <select class="form-control input-inline input-xs" name="gender">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="field">Birthdate</td>
                                <td>
                                    <select class="form-control input-inline input-xs" name="day">
                                        <option value="04" selected>04</option>
                                    </select>
                                    -
                                    <select class="form-control input-inline input-xs" name="month">
                                        <option value="11" selected>11</option>
                                    </select>
                                    -
                                    <select class="form-control input-inline input-xs" name="year">
                                        <option value="1989" selected>1989</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="field">Language</td>
                                <td>
                                    <select class="form-control input-inline input-xs" name="language">
                                        <option value="" selected>English</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- end table -->
            </div>
            <!-- end profile-info -->
        </div>
        <!-- end profile-right -->
    </div>
    <!-- end profile-section -->
</div>
<!-- end profile-container -->
@endsection