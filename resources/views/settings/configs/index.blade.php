@extends('page')
@section('title', 'Configs')
@section('content_header')
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li><a href="{{ url('/') }}">Settings</a></li>
    <li class="active">Configs</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Configs <small>Form</small></h1>
<!-- end page-header -->
@endsection
@section('content')
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12 ui-sortable">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Form Configs</h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('id' => 'MyForm', 'enctype'=>"multipart/form-data",'name'=>'MyForm','method'=>'post', 'class'=>'form-horizontal')) }}
                <!-- text input -->
                <div class="form-group">
                    <label class="col-md-3 control-label">Sitename</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="RCTI+" name="sitename" value="{{$configs->sitename}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Sitename First Word</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="RCTI+" name="sitename_part1" value="{{$configs->sitename_part1}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Sitename Second Word</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="RCTI+ 1.0" name="sitename_part2" value="{{$configs->sitename_part2}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Sitename Short (2/3 Characters)</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="R+" maxlength="2" name="sitename_short" value="{{$configs->sitename_short}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Site Description</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Description in 140 Characters" maxlength="140" name="site_description" value="{{$configs->site_description}}">
                    </div>
                </div>
                <!-- checkbox -->
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="sidebar_search" @if($configs->sidebar_search) checked @endif>
                                Show Search Bar
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="show_messages" @if($configs->show_messages) checked @endif>
                                Show Messages Icon
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="show_notifications" @if($configs->show_notifications) checked @endif>
                                Show Notifications Icon
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="show_tasks" @if($configs->show_tasks) checked @endif>
                                Show Tasks Icon
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="show_rightsidebar" @if($configs->show_rightsidebar) checked @endif>
                                Show Right SideBar Icon
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="body_small_text" @if($configs->body_small_text) checked @endif>
                                Body small text
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="dark_mode" @if($configs->dark_mode) checked @endif>
                                Dark Mode
                            </label>
                        </div>
                    </div>
                </div>
                <!-- select -->
                <div class="form-group">
                    <label class="col-md-3 control-label">Navbar Variants</label>
                    <div class="col-md-9">
                        <select class="form-control" name="navbar_variants">
                            @foreach($variantsnav as $name=>$property)
                            <option value="{{ $property }}" @if($configs->navbar_variants == $property) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Skin Color</label>
                    <div class="col-md-9">
                        <select class="form-control" name="skin">
                            @foreach($skins as $name=>$property)
                            <option value="{{ $property }}" @if($configs->skin == $property) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Layout</label>
                    <div class="col-md-9">
                        <select class="form-control" name="layout">
                            @foreach($layouts as $name=>$property)
                            <option value="{{ $property }}" @if($configs->layout == $property) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Layout</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="To send emails to others via SMTP" maxlength="100" name="default_email" value="{{$configs->default_email}}">
                    </div>
                </div><!-- comment -->
                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="button" class="btn btn-sm btn-primary m-r-5 btn-action-submit"><i class="fa fa-save"></i> Save</button>
                        <a href="{{ url('/') }}" class="btn btn-sm btn-default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('button.btn-action-submit').click(function(e) {
            $(this).html('<i class="fa fa-spinner fa-spin"></i> Processing...');
            $(this).prop('disabled', true);
            e.preventDefault();
            var _form = $("form#MyForm");
            var formData = new FormData(_form[0]);
            var _method = 'POST';
            var _url = "{{ route('settings.configs.store') }}";
            $.ajax({
                url: _url,
                type: _method,
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                success: function(result) {
                    if (result.success) {
                        swal("Updated!", "Your role has been updated.", "success");
                    }
                    $('button.btn-action-submit').html('<i class="fa fa-save"></i> Save');
                    $('button.btn-action-submit').prop('disabled', false);
                },
                error: function(err) {
                    $.each(JSON.parse(err.responseText).message, function(i, error) {
                        var _field = $(document).find('[name="' + i + '"]');
                        _field.addClass('is-invalid');
                        var el = $(document).find('[class="invalid-feedback invalid-' + i + '"]');
                        el.css('display', 'block');
                        el.text(error[0]);
                    });
                    $('button.btn-action-submit').html('<i class="fa fa-save"></i> Save');
                    $('button.btn-action-submit').prop('disabled', false);
                }
            });
        });
    });
</script>
@endsection