<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Str;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Auth;

class UsersController extends Controller
{
    //
    public function index()
    {
        $roles = \App\Model\Roles::pluck('name', 'id');
        return view('settings.users.index', [
            'roles' => $roles
        ]);
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select(['users.id', 'users.name', 'users.email', 'roles.name AS role_name', 'users.job_title'])
                ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
                ->whereNull('deleted_at');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="View" class="btn btn-xs btn-icon btn-circle btn-success btn-action-view"><i class="fa fa-eye"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="btn btn-xs btn-icon btn-circle btn-warning btn-action-edit"><i class="fa fa-pencil"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-xs btn-icon btn-circle btn-danger btn-action-delete"><i class="fa fa-trash"></i></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->orWhere('users.name', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('users.email', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('roles.name', 'LIKE', "%" . Str::lower($search['value']) . "%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function store(Request $request, User $user)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'role_id' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:8', 'same:password'],
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'job_title' => $request->job_title,
            'password' => Hash::make($request->password),
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        if ($data->id) {
            return response()->json([
                'success' => true,
                'message' => 'Add user success'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Add user failure'
            ]);
        }
    }

    public function destroy(Request $request, User $user)
    {
        $user = Auth::user();
        User::where('id', $request->id)->update([
            'deleted_by' => $user->id,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json(['success' => true]);
    }

    public function update(Request $request, User $user)
    {
        $field = [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($request->id)],
        ];

        $dataUpdate = [
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'job_title' => $request->job_title,
        ];

        if ($request->password || $request->password_confirmation) {
            $field['password'] = ['required', 'string', 'min:8', 'confirmed'];
            $field['password_confirmation'] = ['required', 'string', 'min:8', 'same:password'];
            $dataUpdate['password'] = Hash::make($request->password);
        }

        $validator = \Validator::make($request->all(), $field);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        User::where('id', $request->id)->update($dataUpdate);
        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    public function edit($id)
    {
        $Users = User::find($id);
        return response()->json($Users);
    }

    public function show($id)
    {
        $Users = User::find($id);
        return response()->json($Users);
    }

    public function profile()
    {
        return view('settings.users.profile');
    }
}
