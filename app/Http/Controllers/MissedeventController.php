<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Missedevent;
use DataTables;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Validation\Rule;
use ConfigsHelper;
use phpseclib3\Net\SSH2;

class MissedeventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('masters.missedevent.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Missedevent::select(['id', 'name', 'filename', 'filename_path'])
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="View" class="btn btn-xs btn-icon btn-circle btn-success btn-action-view"><i class="fa fa-eye"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="btn btn-xs btn-icon btn-circle btn-warning btn-action-edit"><i class="fa fa-pencil"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-xs btn-icon btn-circle btn-danger btn-action-delete"><i class="fa fa-trash"></i></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->orWhere('name', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename_path', 'LIKE', "%" . Str::lower($search['value']) . "%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Missedevent $user)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'filename' => ['required', 'string', 'max:255', 'unique:r_missedevent'],
            'filename_path' => ['required', 'string', 'max:500']
        ]);

        $user = Auth::user();
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        $data = Missedevent::create([
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'created_by' => $user->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        if ($data->id) {
            return response()->json([
                'success' => true,
                'message' => 'Add user success'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Add user failure'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Missedevent  $missedevent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Users = Missedevent::find($id);
        return response()->json($Users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Missedevent  $missedevent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Users = Missedevent::find($id);
        return response()->json($Users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Missedevent  $missedevent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Missedevent $missedevent)
    {
        $field = [
            'name' => ['required', 'string'],
            'filename' => ['required', 'string', 'max:255', Rule::unique('r_missedevent')->ignore($request->id)],
            'filename_path' => ['required', 'string'],
        ];
        $user = Auth::user();
        $dataUpdate = [
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'updated_by' => $user->id,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $validator = \Validator::make($request->all(), $field);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        Missedevent::where('id', $request->id)->update($dataUpdate);
        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Missedevent  $missedevent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Missedevent $missedevent)
    {
        $user = Auth::user();
        Missedevent::where('id', $request->id)->update([
            'deleted_by' => $user->id,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json(['success' => true]);
    }

    public function strstop()
    {
        $host = ConfigsHelper::getByKey('ssh_host_osiris');
        $port = ConfigsHelper::getByKey('ssh_port');
        $username = ConfigsHelper::getByKey('ssh_username');
        $password = ConfigsHelper::getByKey('ssh_password');

        // $ssh = new SSH2($host, $port);
        // if (!$ssh->login($username, $password)) {
        //     throw new \Exception('Login failed');
        // } else {
        //     $ssh->setTimeout(1);
        //     $ssh->read();
        //     echo $ssh->exec('ping jigen2.unregister.xyz');
        // }
        // die;
        $missedevents = Missedevent::whereNull('deleted_at')->get();
        return view('operations.missedevent.strstop', [
            'missedevents' => $missedevents
        ]);
    }
}
