<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Servers;
use DataTables;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Validation\Rule;
use ConfigsHelper;

class ServersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('masters.servers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Servers::select(['id', 'name', 'filename', 'filename_path'])
                ->whereNull('deleted_at');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="View" class="btn btn-xs btn-icon btn-circle btn-success btn-action-view"><i class="fa fa-eye"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="btn btn-xs btn-icon btn-circle btn-warning btn-action-edit"><i class="fa fa-pencil"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-xs btn-icon btn-circle btn-danger btn-action-delete"><i class="fa fa-trash"></i></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->orWhere('name', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename_path', 'LIKE', "%" . Str::lower($search['value']) . "%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Servers $user)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'filename' => ['required', 'string', 'max:255', 'unique:r_servers'],
            'filename_path' => ['required', 'string', 'max:500']
        ]);

        $user = Auth::user();
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        $data = Servers::create([
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'created_by' => $user->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        if ($data->id) {
            return response()->json([
                'success' => true,
                'message' => 'Add user success'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Add user failure'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servers  $servers
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Users = Servers::find($id);
        return response()->json($Users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servers  $servers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Users = Servers::find($id);
        return response()->json($Users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servers  $servers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Servers $servers)
    {
        $field = [
            'name' => ['required', 'string'],
            'filename' => ['required', 'string', 'max:255', Rule::unique('r_servers')->ignore($request->id)],
            'filename_path' => ['required', 'string'],
        ];
        $user = Auth::user();
        $dataUpdate = [
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'updated_by' => $user->id,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $validator = \Validator::make($request->all(), $field);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        Servers::where('id', $request->id)->update($dataUpdate);
        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servers  $servers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Servers $servers)
    {
        $user = Auth::user();
        Servers::where('id', $request->id)->update([
            'deleted_by' => $user->id,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json(['success' => true]);
    }


    public function strstop($slug)
    {
        $host = ConfigsHelper::getByKey('ssh_host_osiris');
        $port = ConfigsHelper::getByKey('ssh_port');
        $username = ConfigsHelper::getByKey('ssh_username');
        $password = ConfigsHelper::getByKey('ssh_password');

        // $ssh = new SSH2($host, $port);
        // if (!$ssh->login($username, $password)) {
        //     throw new \Exception('Login failed');
        // } else {
        //     $ssh->setTimeout(1);
        //     $ssh->read();
        //     echo $ssh->exec('ping jigen2.unregister.xyz');
        // }
        // die;
        $servers = Servers::whereNull('deleted_at')->get();
        $server = Servers::select('*')->where('filename', $slug)->first();
        return view('operations.servers.strstop', [
            'servers' => $servers,
            'detail' => $server
        ]);
    }

    public function start(Request $request)
    {
        return response()->json(['success' => true, 'id' => $request->id]);
    }
}
