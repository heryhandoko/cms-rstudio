<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Logolive;
use DataTables;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Validation\Rule;

class LogoliveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('masters.logolive.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Logolive::select(['id', 'name', 'filename', 'filename_path'])
                ->whereNull('deleted_at');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="View" class="btn btn-xs btn-icon btn-circle btn-success btn-action-view"><i class="fa fa-eye"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="btn btn-xs btn-icon btn-circle btn-warning btn-action-edit"><i class="fa fa-pencil"></i></a> ';
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-xs btn-icon btn-circle btn-danger btn-action-delete"><i class="fa fa-trash"></i></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->orWhere('name', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename', 'LIKE', "%" . Str::lower($search['value']) . "%")
                                ->orWhere('filename_path', 'LIKE', "%" . Str::lower($search['value']) . "%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Logolive $user)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'filename' => ['required', 'string', 'max:255', 'unique:r_logolive'],
            'filename_path' => ['required', 'string', 'max:500']
        ]);

        $user = Auth::user();
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        $data = Logolive::create([
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'created_by' => $user->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        if ($data->id) {
            return response()->json([
                'success' => true,
                'message' => 'Add user success'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Add user failure'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Logolive  $logolive
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Users = Logolive::find($id);
        return response()->json($Users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Logolive  $logolive
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Users = Logolive::find($id);
        return response()->json($Users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Logolive  $logolive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logolive $logolive)
    {
        $field = [
            'name' => ['required', 'string'],
            'filename' => ['required', 'string', 'max:255', Rule::unique('r_logolive')->ignore($request->id)],
            'filename_path' => ['required', 'string'],
        ];
        $user = Auth::user();
        $dataUpdate = [
            'name' => $request->name,
            'filename' => $request->filename,
            'filename_path' => $request->filename_path,
            'description' => $request->description,
            'updated_by' => $user->id,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $validator = \Validator::make($request->all(), $field);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->toArray()
            ], 422);
        }

        Logolive::where('id', $request->id)->update($dataUpdate);
        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Logolive  $logolive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Logolive $logolive)
    {
        $user = Auth::user();
        Logolive::where('id', $request->id)->update([
            'deleted_by' => $user->id,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json(['success' => true]);
    }
}
