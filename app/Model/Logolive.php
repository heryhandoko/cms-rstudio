<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Spatie\Permission\Traits\HasRoles;

class Logolive extends Model implements Authenticatable
{
    use AuthenticableTrait;
    use HasRoles;
    //
    protected $table = 'r_logolive';

    protected $fillable = [
        'id',
        'name',
        'filename',
        'filename_path'
    ];
}
