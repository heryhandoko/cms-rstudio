<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Spatie\Permission\Traits\HasRoles;

class Configs extends Model implements Authenticatable
{
    use AuthenticableTrait;
    use HasRoles;

    protected $fillable = [
        "key", "value"
    ];

    public static function getByKey($key) {
        $row = Configs::where('key', $key)->first();
        if (isset($row->value)) {
            return $row->value;
        } else {
            return false;
        }
    }

    public static function getAll() {
        $configs = array();
        $configs_db = Configs::all();
        foreach ($configs_db as $row) {
            $configs[$row->key] = $row->value;
        }
        return (object) $configs;
    }

}
