<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Spatie\Permission\Traits\HasRoles;

class Roles extends Model implements Authenticatable
{
    use AuthenticableTrait;
    use HasRoles;

    protected $fillable = [
        'name',
        'label',
        'description',
        'guard_name'
    ];

    public function storeData($input)
    {
        return static::create([
            'name' => $input['name'],
            'label' => $input['label'],
            'description' => $input['description'],
            'guard_name' => $input['name'],
        ]);
    }
}
