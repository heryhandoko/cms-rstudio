<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Spatie\Permission\Traits\HasRoles;

class Menus extends Model implements Authenticatable
{
    use AuthenticableTrait;
    use HasRoles;

    protected $table = 'menus';
    protected $fillable = [
        'module_id',
        'hierarchy',
        'parent'
    ];

    public function storeData($input) {
        return static::create([
                    'module_id' => $input['module_id'],
                    'hierarchy' => 0,
                    'parent' => 0,
        ]);
    }

}
